# Generated by Django 2.2.7 on 2020-11-05 14:58

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', models.CharField(max_length=280)),
                ('nomeAutor', models.CharField(max_length=120)),
                ('data_publicacao', models.DateTimeField(verbose_name='Data de publicação')),
                ('pergunta', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Pergunta')),
            ],
        ),
    ]
