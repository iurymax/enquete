import datetime
from django.test import TestCase
from django.utils import timezone
from django.urls import reverse
from .models import Pergunta

class PerguntaTeste(TestCase):
    def test_publicada_recentemente_com_pergunta_no_futuro(self):
        """
        O metodo publicada_recentemente precisa retornar FALSE quando se
        tratar de perguntas com data de publicacao no futuro.
        """
        data = timezone.now() + datetime.timedelta(seconds=1)
        pergunta_futura = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_futura.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_anterior_a_24h_no_passado(self):
        """
        O metodo publicada_recentemente precisa retornar FALSE quando Se
        tratar de uma data de publicacao anterior a 24hs no passado.
        """
        data = timezone.now() - datetime.timedelta(days=1, seconds=1)
        pergunta_no_passado = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_no_passado.publicada_recentemente(), False)

    def test_publicada_recentemente_com_data_nas_ultimas_24hs(self):
        """
        O metodo publicada_recentemente precisa retornar TRUE quando Se
        tratar de uma data de publicacao dentro das ultimas 24hs.
        """
        data = timezone.now() - datetime.timedelta(hours=23,minutes=59, seconds=59)
        pergunta_ok = Pergunta(data_publicacao = data)
        self.assertIs(pergunta_ok.publicada_recentemente(), True)

def criar_pergunta(texto, dias):
    """
    Função para a criação de uma pergunta para texto e data
    da com uma variação de dias publicação.
    """
    data = timezone.now() + datetime.timedelta(days=dias)
    return Pergunta.objects.create(texto=texto, data_publicacao=data)

class ViewIndexTeste(TestCase):
    def test_sem_perguntas_cadastradas(self):
        """
        Teste da index view quando não houverem perguntas cadastradas
        """
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Não há Enquetes Cadastradas até o Momento")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], [])

    def test_com_pergunta_no_passado(self):
        """
        Teste da IndexView exibindo perguntas do passado.
        """

        criar_pergunta(texto='Pergunta no passado', dias=-20)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], ['<Pergunta: Pergunta no passado>'])

    def test_com_pergunta_no_futuro(self):
        """
        Pergunta fita no futuro nao deve ser exibida
        """
        criar_pergunta(texto='Pergunta no futuro', dias=1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Não há Enquetes Cadastradas até o Momento")
        self.assertQuerysetEqual(resposta.context['ultimas_perguntas'], [])

    def test_pergunta_no_passado_e_no_futuro(self):
        """
        Perguntas com data de publicação no passado são exibidas e com
        data dew publicação no futuro são omitidas.
        """
        criar_pergunta(texto='Pergunta no passado', dias=-1)
        criar_pergunta(texto='Pergunta no futuro', dias=1)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(
            resposta.context['ultimas_perguntas'],
            ['<Pergunta: Pergunta no passado>'])


    def test_duas_perguntas_no_passado(self):
        """
        Exibe normalmente mais de uma Pergunta com data
        de publicação no passado.
        """
        criar_pergunta(texto='Pergunta no passado 1', dias=-1)
        criar_pergunta(texto='Pergunta no passado 2', dias=-5)
        resposta = self.client.get(reverse('main:index'))
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, "Pergunta no passado")
        self.assertQuerysetEqual(
            resposta.context['ultimas_perguntas'],
            ['<Pergunta: Pergunta no passado 1>',
            '<Pergunta: Pergunta no passado 2>']
        )

class DetalhesViewTeste(TestCase):
    def test_pergunta_no_futuro(self):
        """
        Deverá retornar um erro 404 ao indicar uma pergunta com data no
        futuro
        """
        pergunta_no_futuro = criar_pergunta(texto="Pergunta no futuro", dias=5)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta_no_futuro.id,])
        )
        self.assertEqual(resposta.status_code, 404)

    def test_pergunta_no_passado(self):
        """
        Deverá exibir normalmente uma pergunta com data no
        passado
        """
        pergunta = criar_pergunta(texto="Pergunta no Passado", dias=-5)
        resposta = self.client.get(
            reverse('main:detalhes', args=[pergunta.id,])
        )
        self.assertEqual(resposta.status_code, 200)
        self.assertContains(resposta, pergunta.texto)




