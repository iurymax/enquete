from django.contrib import admin
from .models import Post, Fotos, Comentario, Account, Estrelas
# Register your models here.
admin.site.register(Post)
admin.site.register(Comentario)
admin.site.register(Fotos)
admin.site.register(Account)
admin.site.register(Estrelas)
