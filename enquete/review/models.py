from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils import timezone
import datetime

# Create your models here.
class MyAccountManager(BaseUserManager):
	def create_user(self, email, username, cpf, password=None):
		if not email:
			raise ValueError('Users must have an email address')
		if not username:
			raise ValueError('Users must have a username')
		

		user = self.model(
			email=self.normalize_email(email),
			username=username,
		)

		user.set_password(password)
		user.save(using=self._db)
		return user

	def create_superuser(self, email, username, cpf, password):
		user = self.create_user(
			email=self.normalize_email(email),
			password=password,
			username=username,
			cpf=cpf,
		)
		user.is_admin = True
		user.is_staff = True
		user.is_superuser = True
		user.save(using=self._db)
		return user

class Account(AbstractBaseUser):
	email = models.EmailField(verbose_name="email", max_length=60, unique=True)
	username = models.CharField(max_length=30, unique=True)
	cpf = models.CharField(max_length=30)
	date_joined = models.DateTimeField(verbose_name='date joined', auto_now_add=True)
	last_login = models.DateTimeField(verbose_name='last login', auto_now=True)
	is_admin = models.BooleanField(default=False)
	is_active = models.BooleanField(default=True)
	is_staff = models.BooleanField(default=False)
	is_superuser = models.BooleanField(default=False)
	photo = models.ImageField(upload_to='users')

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['username']

	objects = MyAccountManager()

	def __str__(self):
		return self.email

	# For checking permissions. to keep it simple all admin have ALL permissons
	def has_perm(self, perm, obj=None):
		return self.is_admin

	# Does this user have permission to view this app? (ALWAYS YES FOR SIMPLICITY)
	def has_module_perms(self, app_label):
		return True


class Post(models.Model):
	titulo = models.CharField(max_length=128)
	descricao = models.CharField(max_length=200)
	data_publicacao = models.DateTimeField('Data de publicação')
	total_votos = models.IntegerField(default=0)
	user = models.ForeignKey(Account, on_delete=models.CASCADE)
	mediaEstrelas = models.DecimalField(default=0, max_digits=3, decimal_places=2)
	def __str__(self):
		return self.titulo
	def publicada_recentemente(self):
		agora = timezone.now()
		return agora-datetime.timedelta(days=1) <= self.data_publicacao <= agora

class Comentario(models.Model):
    texto = models.CharField(max_length=280)
    nomeAutor = models.CharField(max_length = 120)
    data_publicacao = models.DateTimeField('Data de publicação')
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='postComentario')
    def __str__(self):
        return self.texto

class Fotos(models.Model):
    titulo = models.CharField(max_length=128)
    data_publicacao = models.DateTimeField('Data de publicação')
    image = models.ImageField(upload_to='image_review')
    nota = models.IntegerField(default = 0)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='postImagens')
    def __str__(self):
        return self.titulo

class Estrelas(models.Model):
    estrelas = models.IntegerField(default = 0)
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='postEstrelas')
    def __str__(self):
        return str(self.estrelas)