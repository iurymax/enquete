from django.urls import path
from . import views
from django.contrib.auth import logout
from django.conf.urls import url

from django.contrib.auth.views import LogoutView

app_name = 'review'
urlpatterns = [
    path('review/', views.IndexView.as_view(), name='reviews'),
    #path('<int:pk>', views.IndexView.as_view(), name='index'),
    path('review/<int:pk>',
        views.DetalhesView.as_view(), name='detalhes'
    ),
    path('review/<int:pk>/comentario',
        views.DetalhesComentarioView.as_view(), name='detalhes_comentario'
    ),
    path('review/<int:pk>/resultado',
        views.ResultadoView.as_view(), name='resultado'
    ),
    path('review/<int:id_post>/votacao',
        views.votacao, name='votacao'
    ),

    path('review/<int:pk>/criar_post',
        views.CriarPostView.as_view(), name='criar_post'
    ),
    path('review/<int:id_user>/form_post',
        views.formPost, name='form_post'
    ),


    path('review/cadastro',
        views.iniciarCadastro, name='cadastro'
    ),
    path('review/acessarsistema',
        views.acessarSistema, name='acessarsistema'
    ),
    path('review/formcadastro',
        views.acessarCadastro, name='formcadastro'
    ),

    #url(r'^sign-out/$', logout, {'template_name': 'index.html', 'next_page': '/'}, name='sign-out'),
    #url(r'^logout$', LogoutView.as_view(),  name='logout'),


    path('review/logout',
        views.logout, name='logout'
    ),


    path('review/dashboard',
        views.acessarDashboard , name='dashboard'
    ),

    path('review/login',
        views.login, name='irtelalogin'
    ),
    path('review/realizarlogin',
        views.realizarLogin, name='realizarlogin'
    ),

    path('review/<int:id_game>/game',
        views.openGame, name='opengame'
    ),

]