from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from .models import Account, Post, Fotos, Comentario, Estrelas
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login as auth_login, logout as django_logout

from django.contrib.auth.views import LogoutView

class IndexView(generic.ListView):
    template_name = 'review/home.html'
    context_object_name = 'all_posts'
    def get_queryset(self):
        queryset = {
            'allposts': Post.objects.filter(data_publicacao__lte=timezone.now()).order_by(
            '-data_publicacao'),
            'nomePainel': 'Ir Para O Painel',
            'nomeCadastrese': 'Cadastre-se',
        }
        return queryset


def formPost(request, id_user):
    return render (request, 'user/create_post.html', {"userId": id_user })


class CriarPostView(generic.ListView):
    model = Post
    template_name = 'review/dashboard.html'
    context_object_name = 'usuarioCriarPost'


    def post(self, request, *args, **kwargs):
        usuario = Account.objects.get(pk=request.user.id)
        postagem = Post.objects.create(user=usuario,
        titulo = request.POST['titulo'],
        descricao=request.POST['descricao'],
        data_publicacao=timezone.now(),
        )

        img1 = None
        img2 = None
        img3 = None

        if request.FILES['photo'] is not None:
            img1 = request.FILES['photo']
        if request.FILES['photo2'] is not None:
            img2 = request.FILES['photo2']
        if request.FILES['photo3'] is not None:
            img3 = request.FILES['photo3']

        if img1 is not None:
            foto = Fotos(titulo=postagem.titulo + '-1',data_publicacao=timezone.now(), image=img1, post=postagem)
            foto.save()
        if img2 is not None:
            foto2 = Fotos(titulo=postagem.titulo + '-2',data_publicacao=timezone.now(), image=img2, post=postagem)
            foto2.save()
        if img3 is not None:
            foto3 = Fotos(titulo=postagem.titulo + '-3',data_publicacao=timezone.now(), image=img3, post=postagem)
            foto3.save()

        #post.postImagens.add(foto)
        #post.postImagens.add(foto2)
        postagem.mediaEstrelas = 1


        try:
            postagem.save()
        except (KeyError, Post.DoesNotExist):
            return render(request, 'review/dashboard.html', {
                'custom': usuario,
                'error_message': "Houve um erro ao tentar criar o post!"
            })
        else:
            return HttpResponseRedirect(
                reverse('review:detalhes', args=(postagem.id,))
            )

    def get_queryset(self):
        postObj = Post.objects.get(id=self.kwargs['pk'])
        estrelasRelated = postObj.postEstrelas.all()
        somaEstremas = 0
        for estrelas in estrelasRelated:
            somaEstremas += estrelas.estrelas
        if estrelasRelated.count() == 0:
            mediaEstrelas = 1
        else:
            mediaEstrelas = somaEstremas/estrelasRelated.count()
        #vari = Post.objects.get(id=self.kwargs['pk'])
        postObj.mediaEstrelas = round(mediaEstrelas)
        postObj.save()
        queryset = {'post': postObj,
        'media_estrelas': mediaEstrelas,
        'n' : range(5),
        'qntVotos': estrelasRelated.count(),
        'comentsRelated': postObj.postComentario.all().order_by('-data_publicacao')
        }
        return queryset


class DetalhesView(generic.ListView):
    model = Post
    template_name = 'review/detalhes.html'
    context_object_name = 'postDetalhes'


    def post(self, request, *args, **kwargs):
        post = Post.objects.get(id=self.kwargs['pk'])
        if request.method == 'POST':
            comentario = request.POST['comentario']

        try:
            novo_comentario = Comentario(texto = comentario, nomeAutor = 'Anonimo',
            data_publicacao=timezone.now(), post=post)
        except (KeyError, Comentario.DoesNotExist):
            return render(request, 'review/detalhes.html', {
                'post': post,
                'error_message': "Comentário Inválido!"
            })
        else:
            if novo_comentario is not None:
                novo_comentario.save()

            return HttpResponseRedirect(
                reverse('review:detalhes', args=(post.id,))
            )

    def get_queryset(self):
        postObj = Post.objects.get(id=self.kwargs['pk'])
        estrelasRelated = postObj.postEstrelas.all()
        somaEstremas = 0
        for estrelas in estrelasRelated:
            somaEstremas += estrelas.estrelas
        if estrelasRelated.count() == 0:
            mediaEstrelas = 1
        else:
            mediaEstrelas = somaEstremas/estrelasRelated.count()
        #vari = Post.objects.get(id=self.kwargs['pk'])
        postObj.mediaEstrelas = round(mediaEstrelas)
        postObj.save()
        queryset = {'post': postObj,
        'media_estrelas': mediaEstrelas,
        'n' : range(5),
        'qntVotos': estrelasRelated.count(),
        'comentsRelated': postObj.postComentario.all().order_by('-data_publicacao')
        }
        return queryset

class DetalhesComentarioView(generic.DetailView):
    model = Comentario
    template_name = 'review/detalhes_comentario.html'
    def get_queryset(self):
        return Comentario.objects.filter(data_publicacao__lte=timezone.now())

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['now'] = timezone.now()
        return context

class ResultadoView(generic.DetailView):
    model = Post
    template_name = 'review/resultado.html'

def votacao(request, id_post):
    post = get_object_or_404(Post, pk=id_post)
    try:
        opcao_estrelas = Estrelas(estrelas=request.POST['estrelas'], post=post)
    except (KeyError, Estrelas.DoesNotExist):
        return render(request, 'review/detalhes.html', {
            'post': post,
            'error_message': "Selecione uma opção Válida!"
        })
    else:
        if opcao_estrelas is not None:
            opcao_estrelas.save()

        return HttpResponseRedirect(
            reverse('review:detalhes', args=(post.id,))
        )

def detalhesComentario(request, id_comentario):

    post = get_object_or_404(Post, pk=id_comentario)
    try:
        opcao_selecionada = post.opcao_set.get(pk=request.POST['opcao'])
    except (KeyError, Opcao.DoesNotExist):
        return render(request, 'review/detalhes.html', {
            'post': post,
            'error_message': "Selecione uma opção Válida!"
        })
    else:
        return HttpResponseRedirect(
            reverse('review:detalhes', args=(post.id,))
        )





def openGame(request, id_game):
    return render (request, 'games/darkEngineer/index.html')


def acessarSistema(request):
    return render (request, 'user/login.html')

def acessarCadastro(request):
    return render (request, 'user/signup.html')

def acessarDashboard(request):
    custom = Account.objects.get(username=request.user.username)
    return render (request, 'user/dashboard.html', {"custom": custom })

def logout(request):
    django_logout(request)
    return redirect("review:irtelalogin")

def login(request):
    return render(request, 'user/login.html')

def realizarLogin(request):
    if request.method == 'POST':
        user = authenticate(request, username=request.POST['username'],password=request.POST['password'])

        if user is not None:
            custom = Account.objects.get(username = user.username)
            auth_login(request, user)
            return render (request, "user/dashboard.html", {"custom": custom })
            #return redirect('homepage')
        else:
            return render(request,'user/login.html',{'error':'Usuario ou Senha esta incorreto!'})
    else:
        return render(request,'user/login.html')


def iniciarCadastro(request):
    username = request.POST['username']
    password = request.POST['password']
    email = request.POST['email']
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']

    user = Account.objects.create(
    username = request.POST['username'],
    email = request.POST['email'],
    cpf = '',
    )
    user.set_password(request.POST['password'])
    #user.photo = request.POST['photo']

    if request.FILES['photo']:
        user.photo = request.FILES['photo']

    user.last_name = last_name
    user.first_name = first_name
    user.save()

    return render(request, "user/dashboard.html", {'custom': user , 'message': "Usuário criado com sucesso!"})